package main

import (
	"fmt"
	"github.com/joho/godotenv"
	"log"
	"net"
	"net/http"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"
	pb "sergeysynergy/simpleblog/api/proto"
	"sergeysynergy/simpleblog/internal/grpcer"
	"sergeysynergy/simpleblog/internal/models"
	"sergeysynergy/simpleblog/internal/restjson"
)

func init() {
	err := godotenv.Load("./config/.env")
	if err != nil {
		err = godotenv.Load("../../config/.env")
		if err != nil {
			log.Fatal("Error loading .env file")
		}
	}
}

func main() {
	pAddr := os.Getenv("POSTGRES_ADDRESS")
	pPass := os.Getenv("POSTGRES_PASSWORD")
	pUser := os.Getenv("USER")
	conStr := fmt.Sprintf("postgresql://%s:%s@%s/simpleblog?sslmode=disable", pUser, pPass, pAddr)
	cfg := &models.StoreConfig{
		PostgresConnectionString: conStr,
	}

	// Create in memory data store to store and handle models data.
	store := models.NewStore(cfg)

	// Create few posts for debug purpose.
	if os.Getenv("RUN_MODE") == "debug" {
		createRandomPosts(store)
	}

	// Serve REST JSON.
	gin.SetMode(os.Getenv("GIN_MODE"))
	handler := restjson.Handler{
		Engine: &gin.Engine{},
		Store:  store,
	}

	hAddr := os.Getenv("HTTP_ADDRESS")
	httpServer := &http.Server{
		Addr:           hAddr,
		Handler:        handler.Serve(),  // if nil use default http.DefaultServeMux
		ReadTimeout:    time.Second * 10, // max duration reading entire request
		WriteTimeout:   time.Second * 10, // max timing write response
		IdleTimeout:    time.Second * 10, // max time wait for the next request
		MaxHeaderBytes: 1 << 20,          // 2^20 = 128 Kb
	}
	go func() {
		log.Printf("starting http server at: %s\n", httpServer.Addr)
		log.Fatal(httpServer.ListenAndServe())
	}()

	// Serve gRPC.
	s2 := grpc.NewServer()
	srv := &grpcer.Server{
		Store: store,
	}
	pb.RegisterPosterServer(s2, srv)
	gAddr := os.Getenv("GRPC_ADDRESS")
	log.Println("starting gRPC server at:", gAddr)
	l, err := net.Listen("tcp", gAddr)
	if err != nil {
		log.Fatal(err)
	}
	err = s2.Serve(l)
	if err != nil {
		log.Fatal(err)
	}
}

func createRandomPosts(s *models.Store) {
	posts := []*models.Post{
		{
			Title: "Реализация намеченных плановых заданий оказалась чрезвычайно полезной",
			Body:  "Идейные соображения высшего порядка, а также глубокий уровень погружения не даёт нам иного выбора, кроме определения глубокомысленных рассуждений. В рамках спецификации современных стандартов, предприниматели в сети интернет призваны к ответу. Сложно сказать, почему представители современных социальных резервов, которые представляют собой яркий пример континентально-европейского типа политической культуры, будут преданы социально-демократической анафеме.",
			Tags:  []string{"make", "it", "clear"},
		},
		{
			Title: "Реализация намеченных плановых заданий оказалась чрезвычайно полезной",
			Body:  "Идейные соображения высшего порядка, а также глубокий уровень погружения не даёт нам иного выбора, кроме определения глубокомысленных рассуждений. В рамках спецификации современных стандартов, предприниматели в сети интернет призваны к ответу. Сложно сказать, почему представители современных социальных резервов, которые представляют собой яркий пример континентально-европейского типа политической культуры, будут преданы социально-демократической анафеме.",
			Tags:  []string{"make", "it", "clear"},
		},
	}
	for _, post := range posts {
		err := s.Posts.Add(post)
		if err != nil {
			log.Printf("[ERROR] %v\n", err)
		}
	}
}
