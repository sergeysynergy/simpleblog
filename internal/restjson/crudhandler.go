package restjson

// CRUD operations handlers realization using `gin` web framework.

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	"sergeysynergy/simpleblog/internal/models"
)

type Response struct {
	Message interface{} `json:"message,omitempty"`
	Error   string      `json:"error,omitempty"`
}

// ---------------------------------------------------------------------------------------------------------------------

type CRUDHandler struct {
	models.CrudStorer
}

func (c *CRUDHandler) add(ctx *gin.Context) {
	var resp Response
	model := c.Model()

	err := ctx.BindJSON(model)
	if err != nil {
		resp.Error = err.Error()
		ctx.JSON(http.StatusBadRequest, resp)
		return
	}

	err = c.Add(model)
	if err != nil {
		resp.Error = err.Error()
		ctx.JSON(http.StatusBadRequest, resp)
		return
	}

	resp.Message = fmt.Sprintf("Item %s with ID %d has been successfully created", model.GetModelName(), model.GetID())
	ctx.JSON(http.StatusCreated, resp)
}

func (c *CRUDHandler) get(ctx *gin.Context) {
	var resp Response

	// Try to get models list by tag parameter.
	var tags []string
	url := fmt.Sprintf("%s", ctx.Request.URL)
	tagsStart := strings.Index(url, "tags=[")
	if tagsStart > 0 {
		tagsStart += 6 // add search substr length
		tagsStop := strings.Index(url[tagsStart:], "]")
		tags = strings.Split(url[tagsStart:tagsStart+tagsStop], ",")
		resp.Message = c.Get(tags)
		ctx.JSON(http.StatusOK, resp)
		return
	}

	// Get models list without any parameters.
	resp.Message = c.Get(nil)
	ctx.JSON(http.StatusOK, resp)
}

func (c *CRUDHandler) find(ctx *gin.Context) {
	var resp Response

	_id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		resp.Error = err.Error()
		ctx.JSON(http.StatusBadRequest, resp)
		return
	}
	id := uint32(_id)

	item, err := c.Find(id)
	if err != nil {
		resp.Error = err.Error()
		ctx.JSON(http.StatusBadRequest, resp)
		return
	}

	ctx.JSON(http.StatusOK, item)
}

func (c *CRUDHandler) update(ctx *gin.Context) {
	var resp Response
	model := c.Model()

	err := ctx.BindJSON(model)
	if err != nil {
		resp.Error = err.Error()
		ctx.JSON(http.StatusBadRequest, resp)
		return
	}

	_id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		resp.Error = err.Error()
		ctx.JSON(http.StatusBadRequest, resp)
		return
	}
	id := uint32(_id)

	if model.GetID() != id {
		resp.Error = fmt.Sprintf("the ID's in URL and body don't match")
		ctx.JSON(http.StatusBadRequest, resp)
		return
	}

	err = c.Update(model)
	if err != nil {
		resp.Error = err.Error()
		ctx.JSON(http.StatusBadRequest, resp)
		return
	}

	resp.Message = fmt.Sprintf("Item %s with ID %d has been successfully updated", model.GetModelName(), model.GetID())
	ctx.JSON(http.StatusOK, resp)
}

func (c *CRUDHandler) delete(ctx *gin.Context) {
	var resp Response

	_id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		resp.Error = err.Error()
		ctx.JSON(http.StatusBadRequest, resp.Error)
		return
	}
	id := uint32(_id)

	err = c.Delete(id)
	if err != nil {
		resp.Error = err.Error()
		ctx.JSON(http.StatusBadRequest, resp.Error)
		return
	}

	msg := fmt.Sprintf("Item %s with ID %d has been successfully deleted", c.Model().GetModelName(), id)
	ctx.JSON(http.StatusOK, msg)
}
