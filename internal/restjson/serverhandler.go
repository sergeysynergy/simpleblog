package restjson

import (
	"github.com/gin-gonic/gin"
	"sergeysynergy/simpleblog/internal/models"
)

type Handler struct {
	*gin.Engine
	*models.Store
}

func (h *Handler) Serve() *gin.Engine {
	accounts := gin.Accounts{
		"testov": "Passw0rd33",
	}

	handler := gin.New()
	handler.Use(gin.Logger(), gin.Recovery()) // add middleware

	postHandler := CRUDHandler{h.Store.Posts}

	// Create route handlers without authorization needed for Post model.
	post := handler.Group("/post")
	post.GET("", postHandler.get)
	post.GET("/:id", postHandler.find)

	// Create route handlers with needed authorization for Post model.
	postAuth := handler.Group("/post")
	postAuth.Use(gin.BasicAuth(accounts)) // add auth middleware
	postAuth.POST("", postHandler.add)
	postAuth.PUT("/:id", postHandler.update)
	postAuth.DELETE("/:id", postHandler.delete)

	return handler
}
