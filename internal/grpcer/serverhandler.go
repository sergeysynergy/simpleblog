package grpcer

import (
	"context"
	"fmt"
	"strings"

	pb "sergeysynergy/simpleblog/api/proto"
	"sergeysynergy/simpleblog/internal/models"
)

type Server struct {
	*models.Store
}

func (s *Server) Add(_ context.Context, r *pb.AddRequest) (*pb.BasicResponse, error) {
	if strings.TrimSpace(r.Title) == "" || strings.TrimSpace(r.Body) == "" {
		err := fmt.Errorf("title and body can not be empty")
		return nil, err
	}

	post := &models.Post{
		Title: r.Title,
		Body:  r.Body,
		Tags:  r.Tags,
	}
	err := s.Store.Posts.Add(post)
	if err != nil {
		return nil, err
	}

	resp := &pb.BasicResponse{
		Message: fmt.Sprintf("Post with ID %d has been successfully created", post.GetID()),
	}
	return resp, nil
}

func (s *Server) Get(_ context.Context, r *pb.GetRequest) (*pb.GetResponse, error) {
	//_posts := s.Store.Posts.Get(r.Tags).(map[uint32]*models.Post)
	_posts := s.Store.Posts.Get(r.Tags)

	switch _posts.(type) {
	case map[uint32]*models.Post:
		posts := make([]*pb.Post, 0)
		for _, post := range _posts.(map[uint32]*models.Post) {
			posts = append(posts, &pb.Post{
				ID:        post.ID,
				Title:     post.Title,
				Body:      post.Body,
				Tags:      post.Tags,
				CreatedAt: fmt.Sprintf("%s", post.CreatedAt),
				UpdatedAt: fmt.Sprintf("%s", post.UpdatedAt),
			})
		}
		return &pb.GetResponse{
			Posts: posts,
		}, nil
	case map[string]map[uint32]*models.Post:
		byTag := make([]*pb.ByTag, 0)
		posts := make([]*pb.Post, 0)
		for tagTitle, tag := range _posts.(map[string]map[uint32]*models.Post) {
			for _, post := range tag {
				posts = append(posts, &pb.Post{
					ID:        post.ID,
					Title:     post.Title,
					Body:      post.Body,
					Tags:      post.Tags,
					CreatedAt: fmt.Sprintf("%s", post.CreatedAt),
					UpdatedAt: fmt.Sprintf("%s", post.UpdatedAt),
				})
			}
			byTag = append(byTag, &pb.ByTag{
				Tag:   tagTitle,
				Posts: posts,
			})
		}
		return &pb.GetResponse{
			ByTag: byTag,
		}, nil
	}

	return nil, fmt.Errorf("invalid post type")
}

func (s *Server) Find(_ context.Context, r *pb.PostID) (*pb.Post, error) {
	_post, err := s.Store.Posts.Find(r.ID)
	if err != nil {
		return nil, err
	}

	post := _post.(*models.Post)
	resp := &pb.Post{
		ID:        post.ID,
		Title:     post.Title,
		Body:      post.Body,
		Tags:      post.Tags,
		CreatedAt: fmt.Sprintf("%s", post.CreatedAt),
		UpdatedAt: fmt.Sprintf("%s", post.UpdatedAt),
	}

	return resp, nil
}

func (s *Server) Update(_ context.Context, r *pb.UpdateRequest) (*pb.BasicResponse, error) {
	if strings.TrimSpace(r.Title) == "" || strings.TrimSpace(r.Body) == "" {
		err := fmt.Errorf("title and body can not be empty")
		return nil, err
	}

	post := &models.Post{
		ID:    r.ID,
		Title: r.Title,
		Body:  r.Body,
		Tags:  r.Tags,
	}
	err := s.Store.Posts.Update(post)
	if err != nil {
		return nil, err
	}

	resp := &pb.BasicResponse{
		Message: fmt.Sprintf("Post with ID %d has been successfully updated.", post.GetID()),
	}
	return resp, nil
}

func (s *Server) Delete(_ context.Context, r *pb.PostID) (*pb.BasicResponse, error) {
	err := s.Store.Posts.Delete(r.ID)
	if err != nil {
		return nil, err
	}

	msg := &pb.BasicResponse{
		Message: fmt.Sprintf("post with ID %d has been successfully deleted", r.ID),
	}
	return msg, nil
}

func (s *Server) MustEmbedUnimplementedPosterServer() {}
