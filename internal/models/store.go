package models

import (
	_ "github.com/lib/pq"
)

type StoreConfig struct {
	PostgresConnectionString string
}

type Store struct {
	Posts *PostStore
	Cfg   StoreConfig
}

func NewStore(cfg *StoreConfig) *Store {
	psCfg := PostStoreConfig{
		PostgresConnectionString: cfg.PostgresConnectionString,
	}

	s := &Store{
		Posts: NewPostStore(psCfg),
		Cfg:   *cfg,
	}

	return s
}
