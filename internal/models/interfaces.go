package models

type CrudModel interface {
	GetID() uint32
	GetModelName() string
}

type CrudStorer interface {
	Model() CrudModel
	Add(interface{}) error
	Get(interface{}) interface{}
	Find(uint32) (interface{}, error)
	Update(interface{}) error
	Delete(uint32) error
}
