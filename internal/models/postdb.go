package models

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
)

func (p *PostStore) connect() *sql.DB {
	db, err := sql.Open("postgres", p.Cfg.PostgresConnectionString)
	if err != nil {
		panic(err)
	}

	err = db.Ping()
	if err != nil {
		panic(err)
	}

	return db
}

func (p *PostStore) initPosts() {
	db := p.connect()
	defer db.Close()

	_, checkErr := db.Query("select * from posts;")
	if checkErr != nil {

		sqlStatement := `
			CREATE TABLE posts (
			    id bigint PRIMARY KEY,
			    title TEXT NOT NULL ,
			    body TEXT NOT NULL ,
			    tags json ,
			    created_at TIMESTAMP NOT NULL ,
			    updated_at TIMESTAMP NOT NULL
			);
		`
		_, err := db.Query(sqlStatement)
		if err != nil {
			log.Fatalf("Unable to execute the query. %v", err)
		}
		log.Println("Created 'posts' table")
	}
}

func (p *PostStore) updatePosts() {
	p.mu.Lock()
	defer p.mu.Unlock()

	db := p.connect()
	defer db.Close()

	var posts []*Post

	sqlStatement := `SELECT * FROM posts`
	rows, err := db.Query(sqlStatement)
	if err != nil {
		log.Fatalf("Unable to execute the query. %v", err)
	}
	defer rows.Close()

	var maxID uint32
	for rows.Next() {
		var post Post
		var tags []byte
		//var created, updated uint64

		// Unmarshal the row object to post.
		err = rows.Scan(&post.ID, &post.Title, &post.Body, &tags, &post.CreatedAt, &post.UpdatedAt)
		if err != nil {
			log.Fatalf("Unable to scan the row. %v", err)
		}

		err = json.Unmarshal(tags, &post.Tags)
		if err != nil {
			log.Printf("[ERROR] Unable to unmarshal tags for ID %d - %v\n", post.ID, err)
		}

		posts = append(posts, &post)
		maxID = post.ID
	}

	for _, post := range posts {
		p.byID[post.ID] = post
	}

	p.updated = true
	p.countID = maxID
}

func (p *PostStore) insertPost(post *Post) error {
	db := p.connect()
	defer db.Close()

	// Marshal slice into json to store it in Postgres json field type.
	tags, err := json.Marshal(post.Tags)
	if err != nil {
		return fmt.Errorf("unable mashral tags into json: %v", err)
	}

	sqlStatement := `INSERT INTO posts (id, title, body, tags, created_at, updated_at) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`

	var id uint32
	err = db.QueryRow(sqlStatement, post.ID, post.Title, post.Body, tags, post.CreatedAt, post.UpdatedAt).Scan(&id)
	if err != nil {
		return fmt.Errorf("unable to execute the query: %v", err)
	}

	log.Printf("Inserted a single record %v", id)
	return nil
}

func (p *PostStore) updatePost(post *Post) error {
	db := p.connect()
	defer db.Close()

	// Marshal slice into json to store it in Postgres json field type.
	tags, err := json.Marshal(post.Tags)
	if err != nil {
		return fmt.Errorf("unable mashral tags into json: %v", err)
	}

	sqlStatement := `UPDATE posts SET title=$2, body=$3, tags=$4, updated_at=$5 WHERE id=$1`

	res, err := db.Exec(sqlStatement, post.ID, post.Title, post.Body, tags, post.UpdatedAt)
	if err != nil {
		return fmt.Errorf("unable to execute the query: %v", err)
	}

	_, err = res.RowsAffected()
	if err != nil {
		return fmt.Errorf("error while checking the affected rows: %v", err)
	}

	return nil
}

func (p *PostStore) deletePost(id uint32) error {
	db := p.connect()
	defer db.Close()

	sqlStatement := `DELETE FROM posts WHERE id=$1`

	res, err := db.Exec(sqlStatement, id)
	if err != nil {
		return fmt.Errorf("unable to execute the query: %v", err)
	}

	_, err = res.RowsAffected()
	if err != nil {
		return fmt.Errorf("error while checking the affected rows: %v", err)
	}

	return nil
}
