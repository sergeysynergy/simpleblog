package models

import (
	"fmt"
	"strings"
	"sync"
	"time"
)

type Post struct {
	ID        uint32    `json:"id"`
	Title     string    `json:"title" binding:"required"`
	Body      string    `json:"body" binding:"required"`
	Tags      []string  `json:"tags"`
	CreatedAt time.Time `json:"created-at"`
	UpdatedAt time.Time `json:"updated-at"`
}

func (p *Post) GetID() uint32 {
	return p.ID
}

func (p *Post) GetModelName() string {
	return "post"
}

type PostStoreConfig struct {
	PostgresConnectionString string
}

type PostStore struct {
	Cfg        PostStoreConfig
	mu         sync.RWMutex
	countID    uint32
	byID       map[uint32]*Post
	tagToPosts map[string]map[uint32]*Post
	updated    bool // flag is data updated from DB
}

func NewPostStore(cfg PostStoreConfig) *PostStore {
	ps := &PostStore{
		Cfg:        cfg,
		byID:       make(map[uint32]*Post, 0),
		tagToPosts: make(map[string]map[uint32]*Post, 0),
	}
	ps.initPosts()
	ps.updatePosts()

	return ps
}

func (p *PostStore) Model() CrudModel {
	return new(Post)
}

func (p *PostStore) Add(_post interface{}) error {
	p.mu.Lock()
	defer p.mu.Unlock()

	var post *Post
	switch _post.(type) {
	case *Post:
		post = _post.(*Post)
	default:
		return fmt.Errorf("invalide type, need Post")
	}

	p.countID++
	post.ID = p.countID
	post.CreatedAt = time.Now()
	post.Tags = p.tagsToPost(post, post.Tags, nil)

	// Insert new post in DB.
	err := p.insertPost(post)
	if err != nil {
		return fmt.Errorf("failed to insert new post - %v", err)
	}

	// Finally, inserting new post in map.
	p.byID[post.ID] = post

	return nil
}

func (p *PostStore) Get(_tags interface{}) interface{} {
	p.mu.RLock()
	defer p.mu.RUnlock()

	var tags []string
	switch _tags.(type) {
	case []string:
		tags = _tags.([]string)
	default:
		return p.byID
	}

	if tags != nil && len(tags) != 0 {
		posts := make(map[string]map[uint32]*Post)
		for _, tag := range tags {
			if _posts, ok := p.tagToPosts[tag]; ok {
				posts[tag] = _posts
			}
		}
		return posts
	}

	return p.byID
}

func (p *PostStore) Find(id uint32) (interface{}, error) {
	p.mu.RLock()
	defer p.mu.RUnlock()

	post, ok := p.byID[id]
	if !ok {
		err := fmt.Errorf("post with ID %d not found", id)
		return nil, err
	}

	return post, nil
}

func (p *PostStore) Update(_newPost interface{}) error {
	p.mu.Lock()
	defer p.mu.Unlock()

	var newPost *Post
	switch _newPost.(type) {
	case *Post:
		newPost = _newPost.(*Post)
	default:
		return fmt.Errorf("invalide type, need Post")
	}

	post, ok := p.byID[newPost.ID]
	if !ok {
		return fmt.Errorf("post with ID %d not found", newPost.ID)
	}

	title := strings.TrimSpace(newPost.Title)
	if title == "" {
		return fmt.Errorf("title could not be empty")
	}

	body := strings.TrimSpace(newPost.Body)
	if body == "" {
		return fmt.Errorf("body could not be empty")
	}

	tags := p.tagsToPost(post, newPost.Tags, post.Tags)

	pst := &Post{
		ID:        post.ID,
		Title:     title,
		Body:      body,
		Tags:      tags,
		CreatedAt: post.CreatedAt,
		UpdatedAt: time.Now(),
	}

	err := p.updatePost(pst)
	if err != nil {
		return fmt.Errorf("failed to update post - %v", err)
	}

	// Finally, updating post.
	*post = *pst

	return nil
}

func (p *PostStore) Delete(id uint32) error {
	p.mu.Lock()
	defer p.mu.Unlock()

	_, ok := p.byID[id]
	if !ok {
		return fmt.Errorf("post with ID %d not found", id)
	}

	err := p.deletePost(id)
	if err != nil {
		return fmt.Errorf("failed to delete post with ID %d - %v", id, err)
	}

	// Finally, deleting post from map.
	delete(p.byID, id)

	return nil
}

// Need to use mutex lock in calling function.
func (p *PostStore) tagsToPost(post *Post, tags []string, oldTags []string) []string {
	// Unlink current tags from post.
	for _, title := range oldTags {
		posts, ok := p.tagToPosts[title]
		if ok {
			_, kk := posts[post.ID]
			if kk {
				delete(posts, post.ID)
			}
		}
	}

	tagsList := make([]string, 0)

	// Link new tags to post.
	for _, title := range tags {
		posts, ok := p.tagToPosts[title]
		if ok {
			_, kk := posts[post.ID]
			if !kk {
				posts[post.ID] = post
				tagsList = append(tagsList, title)
			}
			continue
		}

		posts = make(map[uint32]*Post, 1)
		posts[post.ID] = post
		p.tagToPosts[title] = posts
		tagsList = append(tagsList, title)
	}

	return tagsList
}
