# Simple blog service

Basic JSON API service example using REST architecture. Service implements basic CRUD operation for Post and Tag models. `Gin` web framework is used for routing and simple authentication.

Easiest way to run service is to use run scripts for `Docker` and `docker-compose`. Otherwise, you have to: run `Postgres` by yourself; create database `simpleblog` in it; change Postgres server address in `config/.env`.

# Quick start

By default, http-server will start at port `:8080`.

Simple service  start using just `Go`:
```
cd cmd/server
go run .
```

Run using `Docker` and `docker-compose` via bash script:
``` 
build/manage.sh start
```

CRUD operations without authorization:

| Method | Path    | Operation      | Query example                                                                                                                                |
|--------|---------|----------------|----------------------------------------------------------------------------------------------------------------------------------------------|
| GET    | /post   | Get posts list | `http://localhost:8080/post`                                                                                           |
| GET    | /post/1 | Get post       | `http://localhost:8080/post/1`                                                                                         |

You can query all posts grouped by tag using `tags` parameter. For example to query all posts with `clear` and `body` tags:
``` 
http://127.0.0.1:8080/post?tags=[clear,body]
```

CRUD operations where authorization `is needed`:
- Authorization type: `Basic Auth`
- Username: `testov`
- Password: `Passw0rd33`

| Method | Path    | Operation      | Query example using CLI tool `curl`                                                                                                                              |
|--------|---------|----------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| POST   | /post   | Add post       | `curl -v -H "Content-Type: application/json" --user testov:Passw0rd33 -d '{"id": 1, "title": "Super post", "body": "Body to body!"}' http://localhost:8080/post`                         |
| PUT    | /post/1 | Update post    | `curl -v -H "Content-Type: application/json" --user testov:Passw0rd33 -d '{"id": 1, "title": "Just a post", "body": "Cake is good, cake is good"}' -X "PUT" http://localhost:8080/post/1` |
| DELETE | /post/1 | Delete post    | `curl -v -H "Content-Type: application/json" --user testov:Passw0rd33 -X "DELETE" http://localhost:8080/post/1`                                                                           |

# gRPC

You can check gRPC API using evans client: `https://github.com/ktr0731/evans`.
``` 
cd api/proto
evans post.proto -p 8081
```

# Models

__Post__ structure fields and types:
- id - uint32
- title - string
- body - string
- tags - []string
- created-at - time
- updated-at - time

# Docker

To run service inside Docker container you should have Docker and `docker-compose` installed in you operating system.    

Build and start container, run service in it:
``` 
build/manage.sh start
```

Stop container and service:
```
build/manage.sh stop
```

Purge service connected data from Docker:
```
build/manage.sh purge
```

# TODO

- Add tests
- Add context timeouts
- Implement the database as plugin
- Add pagination
- Add websockets
