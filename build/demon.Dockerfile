FROM golang:1.17

### Root section #######################################################################################################
ARG USER
RUN useradd -ms /bin/bash $USER

WORKDIR /workdir
COPY . /workdir
RUN go mod tidy
RUN chown $USER:$USER -R /workdir

WORKDIR /workdir/cmd/server
RUN go install .
WORKDIR /workdir

### User section #######################################################################################################
USER $USER
COPY build/demon_bash_aliases /home/$USER/.bash_aliases
ENV TZ="UTC"
CMD ["server"]
